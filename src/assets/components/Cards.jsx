import React from 'react'
import './Cards.css'
export default function Cards(props) {
    // console.log(props.data);
    return (
        <div className='card-container'>{props.data.map((people) => (
            people.people.map((person) => {
                // console.log(member)
                return (
                    <div className="card" key={person.name}>
                        <img src={person.image} />
                        <h2>{person.name}</h2>
                        <p>{person.description}</p>
                        <button>KNOW MORE</button>
                    </div>
                )

            })
        ))}
        </div>
    )
}

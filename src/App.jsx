import React, { Component } from 'react'
import './App.css'
import data from './assets/data/data'
import Cards from './assets/components/Cards'


export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      got: data
    }
  }
  familyButton = (event) => {
    let Buttonvalue = event.target.innerText
    let dataArray = data.filter((family) => {
      if (family.name.toUpperCase() == Buttonvalue) {
        family.name += ' '
        return true
      } else {
        let clone = family.name
        clone = clone.split('')
        if (clone[clone.length - 1] == ' ') {
          clone.pop()
        }
        clone = clone.join('')
        family.name = clone
        return false
      }
    })
    this.setState({
      got: dataArray
    })

    console.log(dataArray);
  }
  searchFilter = (event) => {
    let searchValue = event.target.value
    let clone = JSON.stringify(data)
    clone = JSON.parse(clone)
    let dataArray = clone.map((family) => {
      family.people = family.people.filter((person) => {
        return person.name.toLowerCase().includes(searchValue.toLowerCase())
      })
      return family
    })
    console.log(dataArray);
    this.setState({
      got: dataArray
    })
  }
  render() {
    return (
      <>
        <header className='bg-gradient-to-r from-blue-500 to-violet-500 text-center'>
          <h1>People of GOT👑</h1>
          <input type="text" onKeyUp={this.searchFilter} placeholder="Search the people of GOT" />
        </header>
        <div className='button-container'>
          {data.map((element) => {
            let clone = element.name.split('')
            if (clone[clone.length - 1] == ' ') {
              return <button style={{ background: 'white', color: 'black', border: '1px solid black' }} onClick={this.familyButton} key={element.name}>{element.name.toUpperCase()}</button>
            } else {
              return <button onClick={this.familyButton} key={element.name}>{element.name.toUpperCase()}</button>
            }
          })}
        </div>
        <Cards data={this.state.got} />
      </>
    )
  }
}
